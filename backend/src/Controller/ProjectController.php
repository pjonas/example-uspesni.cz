<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\User;
use App\Entity\Ventilator;
use App\Facade\ProjectFacade;
use App\Facade\UserFacade;
use Dompdf\Dompdf;
use Dompdf\Options;
use FOS\RestBundle\Controller\FOSRestController;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Nette\Utils\Json;
use PHPUnit\Runner\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;


/**
 * Class ProjectController
 *
 * @package App\Controller
 *
 * @Route("/api")
 */
class ProjectController extends BaseController
{
	/**
	 * @var ProjectFacade
	 */
	private $pf;

	/**
	 * @var UserFacade
	 */
	private $uf;

	/**
	 * ProjectController constructor.
	 *
	 * @param ProjectFacade $pf
	 */
	public function __construct(ProjectFacade $pf, UserFacade $uf)
	{
		$this->pf = $pf;
		$this->uf = $uf;
	}


	/**
	 * @Route(path="/project/save", methods={"POST", "OPTIONS"})
	 * @param Request $request
	 * @return JsonResponse
	 * @throws \Exception
	 */
	public function  updateProjectAction(Request $request)
	{
		try{
			$this->isTokenValid($request);
		}catch(JWTDecodeFailureException $e)
		{
			return $this->getJsonResponse(['status' =>'error', 'message' => 'session expired.']);
		}

		if($request->getMethod() === "POST")
		{

		$user = $this->getUserFromToken($request);
		$data = json_decode($request->getContent());

		try{
			if($data === NULL)
			{
				$data = new \stdClass();
				$data->userId = $user->getId();
				$data->title = $request->request->get('title');
				$data->id = $request->request->get('id');
                $storage = $request->request->get('storage');
				$data->storage = trim($storage, '"');
				$data->description = $request->request->get('description');
			}
			/** @var Project $project */
			$project = $this->pf->createProject($data);
			if($project){
				return $this->getJsonResponse(['status' => 'OK','project'=> $project->toArray()]);
			}
			return $this->getJsonResponse(['error' => 'create project failed.']);
		}catch(Exception $e) {
			return $this->getJsonResponse(['error' => 'create project failed. ','message' => $e->getMessage()]);
		}

		}
		return $this->getJsonResponse(['error' => 'aa access deninded.']);
	}


	/**
	 * @Route(path="/project", methods={"POST","OPTIONS"})
	 * @param Request $request
	 * @return JsonResponse
	 * @throws \Exception
	 */
	public function newAction(Request $request): JsonResponse
	{

		if($request->getMethod() === "POST")
		{
			try{
				$this->isTokenValid($request);
			}catch(JWTDecodeFailureException $e)
			{
				return $this->getJsonResponse(['status' =>'error', 'message' => 'session expired.']);
			}

			$user = $this->getUserFromToken($request);
			$data = json_decode($request->getContent());

			try{
				if($data === NULL)
				{
					$data = new \stdClass();
					$data->userId = $user->getId();
					$data->title = $request->request->get('title');
                    $storage = $request->request->get('storage');
                    $data->storage = trim($storage, '"');
					$data->description = $request->request->get('description');
				}
				/** @var Project $project */
				$project = $this->pf->createProject($data);
				if($project){
					return $this->getJsonResponse(['status' => 'OK','project'=> $project->toArray()]);
				}
				return $this->getJsonResponse(['error' => 'create project failed.']);
			}catch(Exception $e) {
				return $this->getJsonResponse(['error' => 'create project failed. ','message' => $e->getMessage()]);
			}
		}
	}


	/**
	 * @Route(path="/projects",name="get_project", methods={"GET"})
	 * @param Request $request
	 * @param $id
	 * @return JsonResponse
	 */
	public function  getProjectsAction(Request $request): JsonResponse
	{
		try{
			$this->isTokenValid($request);
		}catch(JWTDecodeFailureException $e)
		{
			return $this->getJsonResponse(['status' =>'error', 'message' => 'session expired.']);
		}
		/** @var User    $user */
		$user = $this->getUserFromToken($request);
        $result = FALSE;
        if ($user) {
            $result = $this->pf->getUserProjects($user->getId());
        }
		if($result){
			return $this->getJsonResponse(['status' => 'OK', 'data' => $result], 200, ['Access-Control-Allow-Origin' => '*' ]);
		}
		return new JsonResponse(['error' => 'request failed.'], 200,['Access-Control-Allow-Origin' => '*' ]);
	}

	/**
	 * @Route(path="/projects",  methods={"POST","OPTIONS","GET"})
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function getAllProjectsActions(Request $request) : JsonResponse
	{
		$userId = (int) $request->request->get('userId');

		$data = [];
		if($userId) {
			$projects = [];
			try{
				/** @var Project[] $projects */
				$projects = $this->pf->getUserProjects((int) $userId);
				$data = [];
				if($projects) {
					foreach ($projects as $project) {
						$data[] = [
							'id' => $project->getId(),
							'title' => $project->getTitle(),
							'description' => $project->getDescription(),
						];
					}
				}
			}catch( \InvalidArgumentException $e) { }
		}
		$responseData = [
			'status' => 'OK',
			'data' => $data,
		];
		return $this->getJsonResponse( $responseData );
	}

	/**
	 * @Route(path="/project/{id}/save", methods={"POST", "OPTIONS"})
	 * @param Request $request
	 * @param $id
	 * @return JsonResponse
	 * @throws \Exception
	 */
	public function newProjectAction(Request $request, $id): JsonResponse {

			$status = TRUE;
			$messages = [];

		if($request->getMethod() === "POST") {
			$projectId = (int)$id;
			$data = json_decode($request->getContent());

			if ($status) {
				try {
					$this->pf->updateProject($projectId,$data->user->id, $data->title, $data->description, $data->storage);

					return $this->getJsonResponse(['status' => 'OK', 'message' => 'project updated.',], 200, ['Access-Control-Allow-Origin' => '*']);
				}
				catch (Exception $e) {
					return $this->getJsonResponse(['status' => 'failed', 'errors' => $e->getMessage()], 200, ['Access-Control-Allow-Origin' => '*']);
				}
			}
		}
		$messages[] = 'no supported method';
		return $this->getJsonResponse(['status' => 'failed', 'errors' => $messages], 200, ['Access-Control-Allow-Origin' => '*' ]);
	}

	/**
	 * @route(path="/project/{id}/print",name="print_project")
	 * @Method({"POST","OPTIONS","GET"})
	 * @param Request $request
	 * @param int $id id of project
	 * @return Response
	 */
	public function getProjectPrintAction(Request $request, $id): Response{

        if($request->getMethod() === "POST") {
            $storage = $request->request->get('storage');
            $image = $request->request->get('img');
            $requeestData = json_decode(trim($storage, '"'));
            $html = "";
            $resists = [];
            $celkovyOdpor = (float)$requeestData->useky[0]->lutna->computedResist;

            if ($requeestData->useky[0]->resists !== NULL) {
                $resists = $requeestData->useky[0]->resists;
                foreach ($resists as $resist) {
                    foreach($resist->odpor->odpor as $r) {
                        if($r->velikost === $resist->selectedSize) {
                            $celkovyOdpor += (float) $r->hodnota;
                        }

                    }
                }
            }
            if ($requeestData->calculatorData->dul !== NULL) {
                $preparedData = $this->preparedDulData($requeestData->calculatorData->dul);
                $preparedData['title'] = $requeestData->title;
                $preparedData['description'] = $requeestData->description;
                $html = $this->renderView('/project/dul.html.twig', [
                    'title' => "Welcome to our PDF Test",
                    'project' => $preparedData,
                    'ventilator' => $requeestData->useky[0]->ventilator,
                    'lutna' => $requeestData->useky[0]->lutna,
                    'graph' => $image,
                    'resists' => $resists,
                    'celkovyOdpor' => $celkovyOdpor
                ]);
            }
            if ($requeestData->calculatorData->tunel !== NULL) {
                $preparedData = $this->preparedTunelData($requeestData->calculatorData->tunel);
                $preparedData['title'] = $requeestData->title;
                $preparedData['description'] = $requeestData->description;
                $html = $this->renderView('/project/tunel.html.twig', [
                    'title' => "Welcome to our PDF Test",
                    'project' => $preparedData,
                    'ventilator' => $requeestData->useky[0]->ventilator,
                    'lutna' => $requeestData->useky[0]->lutna,
                    'graph' => $image,
                    'resists' => $resists,
                    'celkovyOdpor' => $celkovyOdpor
                ]);
            }

            // Configure Dompdf according to your needs
            $pdfOptions = new Options();
            $pdfOptions->set('defaultFont', 'DejaVu Sans');

            // Instantiate Dompdf with our options
            $dompdf = new Dompdf($pdfOptions);


            // Load HTML to Dompdf
            $dompdf->loadHtml($html);

            // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
            $dompdf->setPaper('A4', 'portrait');

            // Render the HTML as PDF
            $dompdf->render();

            return $this->getFileSendResponse($dompdf->output());
        }
        $messages[] = 'no supported method';
        return $this->getJsonResponse(['status' => 'failed', 'errors' => $messages], 200, ['Access-Control-Allow-Origin' => '*' ]);
	}

    protected function  preparedDulData($fillData)
    {
        $defaultData = [
            'title' => '',
            'description' => '',
            'exhalationCh4' => 0,
            'pripustnakoncentracech4' => 0,
            'exhalationC02' => 0,
            'koncentracech4' => 0,
            'pripustnakoncentraceco2' => 0,
            'koncentraceco2' => 0,
            'predpokladanyPocetPracovnikuN1' => 0,
            'predpokladanyPocetSoucasneProvozovanychStrojuSNaftovymMotoremN2' => 0,
            'casKeSnizeniKoncentrace' => 0,
            'pripustnakoncentraceco2Trhaci' => 0,
            'svetlyPrurez' => 0,
            'maxPrurez' => 0,
            'mernahmotnosthornin' => 0,
            'hmotnostTrhaviny' => 0,
            'delkazabirkytrhaciprace' => 0,
            'delkaVetranehoDila' => 0,
            'koeficientPoruseniRadioaktivniRovnovahy' => 0,
            'exhalationRn' => 0,
            'pripustnakoncentracecRn' => 0,
            'koncentracecRn' => 0,
            'engines' =>[],
            'qv0' => 0,
            'qv1' => 0,
            'qv2' => 0,
            'qv3foukaci' => 0,
            'qv3saci' => 0,
            'qv4' => 0,
            'qv4Hloubeni' => 0,
            'qv5' => 0,
            'qv6' => 0,
            'q3' => 0
        ];

        foreach ($defaultData as $k => $v) {
            if (isset($fillData->computed->$k)) {
                $defaultData[$k] = $fillData->computed->$k;
            }else if (isset($fillData->inputs->$k)) {
                $defaultData[$k] = $fillData->inputs->$k;
            }
        }

        return $defaultData;

    }

    protected function  preparedTunelData($fillData)
    {
        $defaultData = [
            'title' => '',
            'description' => '',
            'celkovaExhalaceCO2' => 0,
            'exhalationC02' => 0,
            'svetlyPrurez' => 0,
            'delkavetranehodila' => 0,
            'delkazabirkytrhaciprace' => 0,
            'dobaPotrebnaKOdvetraniHorniny' => 0,
            'hmotnostOdpaleneTrhaviny' => 0,
            'koncentraceCO2' => 0,
            'mernahmotnosthornin' => 0,
            'mnozstviKonvencnihoCO' => 0,
            'nejvyssiPocetPracovnikuVeSmene' => 0,
            'nejvyssiPripustnaKoncentracePrachu' => 0,
            'predpokladanyPocetPracovnikuN1' => 0,
            'predpokladanyPocetSoucasneProvozovanychStrojuSNaftovymMotoremN2' => 0,
            'pripustnaKoncentraceCO2' => 0,
            'selectedMinimalniPritokVzduchu' => 0,
            'predpokladanyVyvinPrachu' => 0,
            'vzdalenostLutenOdCelby' => 0,
            'engines' =>[],
            'qv0' => 0,
            'qCO2' => 0,
            'qtpSaci' => 0,
            'qtpFoukaci' => 0,
            'qv3saci' => 0,
            'qVyvinPrachu' => 0,
            'qRedeniPrachu' => 0,
            'qPocetPracovniku' => 0
        ];

        foreach ($defaultData as $k => $v) {
            if (isset($fillData->computed->$k)) {
                $defaultData[$k] = $fillData->computed->$k;
            } else {
                if (isset($fillData->inputs->$k)) {
                    $defaultData[$k] = $fillData->inputs->$k;
                }
            }
        }
        return $defaultData;
    }

	/**
	 * @param Request $request
	 * @return bool
	 * @throws JWTDecodeFailureException
	 */
	protected function isTokenValid(Request $request)
	{
		$auth = $request->headers->get('Authorization');
		$tokenString = str_replace("Bearer ", "", $auth);
		$token = $this->uf->checkToken($tokenString);
		if ($token) {
			$this->get('lexik_jwt_authentication.encoder')
				->decode($token->getData());
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * @param Request $request
	 * @return User|null
	 */
	protected function getUserFromToken(Request $request) //: User
	{
		$auth = $request->headers->get('Authorization');
		$tokenString = str_replace("Bearer ", "", $auth);
		$token = $this->uf->checkToken($tokenString);
		if ($token) {
			$decoded = $this->get('lexik_jwt_authentication.encoder')
				->decode($token->getData());
			return $this->uf->findUserById($decoded['data']->id);
		}
		return null;
	}
}
