<?php

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * Class Lutna
 *
 * @package App\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="project")
 */
class Project
{
	use Identifier;


	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\User" ,cascade={"persist"})
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
	 *
	 * @var User
	 */
	public $user;

	/**
	 * @ORM\Column(type="text", name="title", length=255)
	 *
	 * @var string
	 */
	public $title;


	/**
	 * @ORM\Column(type="text", name="description")
	 * @var string
	 */
	public $description;


	/**
	 * @ORM\Column(type="json")
	 * @var string JSON storage
	 */
	public $storage;

	/**
	 * @return User
	 */
	public function getUser(): User
	{
		return $this->user;
	}

	/**
	 * @param User $user
	 */
	public function setUser(User $user): void
	{
		$this->user = $user;
	}

	/**
	 * @return string
	 */
	public function getTitle(): string
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle(string $title): void
	{
		$this->title = $title;
	}

	/**
	 * @return string
	 */
	public function getDescription(): string
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription(string $description): void
	{
		$this->description = $description;
	}

	/**
	 * @return string
	 */
	public function getStorage(): string
	{
		return $this->storage;
	}

	/**
	 * @param string $storage
	 */
	public function setStorage(string $storage): void
	{
		if($storage === NULL)
		{
			$storage = [];
		}
		$this->storage = $storage;
	}


	public function toArray()
	{
		return [
			'id' => $this->getId(),
			'user' => $this->getUser()->toArray(),
			'title' => $this->getId(),
			'description' => $this->getDescription(),
			'storage' => $this->getStorage()
		];
	}
}