<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
	use Identifier;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $surname;

    /**
     * @ORM\Column(type="string")
     */
    private $username;

    /**
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(name="roles",type="array")
     */
    private $roles;
	
	/**
	 * @ORM\Column(name="active", type="boolean", nullable=false)
	 */
	private $isActive = true;
	
	/**
	 * @ORM\OneToMany(targetEntity="Token", mappedBy="user", cascade={"persist", "remove"})
	 */
	private $tokens;
	
	public function __construct()
	{
		$this->tokens = new ArrayCollection();
	}
	
	
	
	public function getId(): int
	{
		return $this->id;
	}

	public function setName(string $name): void
	{
		$this->name = $name;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function setSurname(string $surname): void
	{
		$this->surname = $surname;
	}

	public function getSurname(): string
	{
		return $this->surname;
	}

	public function setUsername(string $username): void
	{
		$this->username = $username;
	}

	public function getUsername(): string
	{
		return $this->username;
	}

	public function setEmail(string $email): void
	{
		$this->email = $email;
	}

	public function getEmail(): string
	{
		return $this->email;
	}

	public function setPassword(string $password): void
	{
		$this->password = $password;
	}

	public function getPassword(): string
	{
		return $this->password;
	}
	
	/**
	 * @return mixed
	 */
	public function getisActive()
	{
		return $this->isActive;
	}
	
	/**
	 * @param mixed $isActive
	 */
	public function setIsActive($isActive): void
	{
		$this->isActive = $isActive;
	}

	public function getRoles()
	{
		$roles = $this->roles;

		if (empty($roles)) {
			$roles[] = 'ROLE_USER';
		}

		return array_unique($roles);
	}

	public function setRoles(iterable $roles): void
	{
		$this->roles = $roles;
	}
	
	public function getSalt()
	{
	}
	
	public function eraseCredentials()
	{
	}
	
	public function addToken(Token $token): self
	{
		$this->tokens[] = $token;
		
		return $this;
	}
	
	public function removeToken(Token $token): bool
	{
		return $this->tokens->removeElement($token);
	}
	
	public function getTokens(): Collection
	{
		return $this->tokens;
	}
	
	public function toArray()
	{
		return [
			'id'=> $this->getId(),
			'fullName'=> $this->name . ' ' . $this->surname,
			'username'=> $this->username,
			'active'=> $this->isActive,
		];
	}
}
