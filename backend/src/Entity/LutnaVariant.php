<?php

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class LutnaVariant
 *
 * @package App\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="lutna_variant")
 */
class LutnaVariant
{
	
	use Identifier;
	
	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Lutna" ,cascade={"persist"})
	 * @ORM\JoinColumn(name="lutna_id", referencedColumnName="id")
	 *
	 * @var Lutna
	 */
	private $lutna;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	private $size;
	
	/**
	 * @ORM\Column(type="string")
	 */
	private $type;
	
	/**
	 * @ORM\Column(type="float")
	 */
	private $value;
	
	/**
	 * @return Lutna
	 */
	public function getLutna()
	{
		return $this->lutna;
	}
	
	/**
	 * @param mixed $lutna
	 */
	public function setLutna(Lutna $lutna)
	{
		$this->lutna = $lutna;
	}
	
	/**
	 * @return mixed
	 */
	public function getSize()
	{
		return $this->size;
	}
	
	/**
	 * @param mixed $size
	 */
	public function setSize($size)
	{
		$this->size = $size;
	}
	
	/**
	 * @return mixed
	 */
	public function getType()
	{
		return $this->type;
	}
	
	/**
	 * @param mixed $type
	 */
	public function setType($type)
	{
		$this->type = $type;
	}
	
	/**
	 * @return mixed
	 */
	public function getValue()
	{
		return $this->value;
	}
	
	/**
	 * @param mixed $value
	 */
	public function setValue($value)
	{
		$this->value = $value;
	}
	
}