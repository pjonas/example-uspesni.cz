<?php

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Lutna
 *
 * @package App\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="lutna")
 */
class Lutna
{
	use Identifier;
	
	/**
	 * @ORM\Column(type="string", length=255)
	 */
	public $name;
	
	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}
	
	/**
	 * @param mixed $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}
	
}