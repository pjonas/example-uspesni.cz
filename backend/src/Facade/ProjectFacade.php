<?php
/**
 * Created by PhpStorm.
 * User: petr
 * Date: 2019-05-06
 * Time: 17:12
 */

namespace App\Facade;


use App\Entity\Project;
use App\Entity\User;
use App\Repository\ProjectRepositoryInterface;
use App\Repository\UserRepositoryInterface;

class ProjectFacade
{

	/**
	 * @var ProjectRepositoryInterface
	 */
	private $pr;
	/**
	 * @var UserRepositoryInterface
	 */
	private $ur;

	/**
	 * ProductFacade constructor.
	 *
	 * @param ProjectRepositoryInterface $pr
	 * @param UserRepositoryInterface $ur
	 */
	public function __construct(ProjectRepositoryInterface $pr, UserRepositoryInterface $ur)
	{
		$this->pr = $pr;
		$this->ur = $ur;
	}

	public function createProject(\stdClass $data)
	{
		$user = $this->ur->findOneActiveById((int)$data->userId);
		if(!$user){
			throw new \Exception('Wrong combination of data.');
		}
		$project = new Project();
		$project->setTitle($data->title);
		$project->setDescription($data->description);
		$project->setUser($user);
		$project->setStorage($data->storage);
		$project = $this->pr->save($project);
		return $project;
	}

	/**
	 * @param \stdClass $data
	 * @return Project
	 * @throws \Exception
	 */
	public function updateProject($projectId, $userId, $title, $description, $storage)
	{
		$project = $this->pr->findById($projectId);
		if (!$project) {
			throw new \Exception('Project not found.');
		}
		/** @var User $user */
		$user = $this->ur->findOneActiveById((int)$userId);
		if (!$user) {
			throw new \Exception('Wrong combination of data.');
		}
		if($user->getId() !== $project->getUser()->getId()) {
			throw new \Exception('Secutrity alert.');
		}
		$project->setTitle($title);
		$project->setDescription($description);
		$project->setStorage(json_encode($storage));

		return $this->pr->save($project);
	}

	public function getUserProjects(int $userId) {
		$user = $this->ur->findOneActiveById($userId);
		if(!$user){
			return FALSE;
		}
		return $this->pr->findUserProject($user);
	}

    public function getAllProjects() {
        return $this->pr->findAll();
    }

	public function getProject(int $id): ?Project
	{
		return $this->pr->findById($id);
	}
}
