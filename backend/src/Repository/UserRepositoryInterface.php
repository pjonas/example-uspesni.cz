<?php

namespace App\Repository;

use App\Entity\User;

interface UserRepositoryInterface
{
	public function findOneById(int $id): ?User;
	
	public function findOneActiveById(int $id): ?User;
	
	public function findOneActiveByUsername(string $username): ?User;
	
	public function save(User $user): User;
}
