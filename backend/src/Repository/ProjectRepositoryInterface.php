<?php

namespace App\Repository;


use App\Entity\Company;
use App\Entity\Project;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

interface ProjectRepositoryInterface
{
	
	public function findById(int $id): ?Project;
	
	public function findUserProject(User $user): iterable;
	
	public function findProjectWholeCompany(Company $company): iterable;
	
	/**
	 * @param Project $project
	 * @return null|Project
	 */
	public function save(Project $project): ?Project;
}
