export default class BaseCalculator {
  /**
   * Qv2 - Exhalace CO2
   * @param q2 Předpokládaná exhalace CO2 v projektovaném důlním díle ( m3s-1 )
   * @param c2 Přípustná koncentrace CO2 v projektovaném důlním díle ( % )
   * @param c3 Koncentrace CO2 ( % )
   * @param q3
   * @returns {number}
   */
  qv2 (q2, c2, c3, q3) {
    q2 = Number(q2)
    c2 = Number(c2)
    c3 = Number(c3)
    q3 = Number(q3)
    return ((q2 + q3) / (c2 - c3)) * 100
  }

  /**
   * q3
   * @param n1 Predpokladany pocet pracovniku
   * @param n2 Predpokladany pocet soucasne provozovanych naftovych stroju
   * @returns {number}
   */
  q3Compute (n1, n2) {
    n1 = Number(n1)
    n2 = Number(n2)
    let q3 = ((0.09 * n1) + (33 * n2)) / 3600
    return q3
  }

  /**
   *
   * @param t Doba potřebná k odvětrání zplodin po trhací práci
   * @param a Hmotnost odpálené trhaviny
   * @param delta Množství konvenčního CO
   * @param s Světlý průřez raženého větraného díla
   * @param l Délka větraného díla k úkrytu osádky
   * @returns {number}
   */
  qv3Foukaci (a, t, delta, s, l) {
    t = Number(t)
    a = Number(a)
    s = Number(s)
    l = Number(l)
    delta = Number(delta)
    let part1 = 0.349 / t
    let part2 = delta * a * (s * s) * (l * l)
    let result = part1 * Math.cbrt(part2)
    if (isNaN(result)) {
      return 0.0
    }
    return result
  }

  /**
   * Qv3S - TP - sací
   *
   * @param a Hmotnost odpálené trhaviny ( kg )
   * @param t Doba potřebná k odvětrání zplodin po trhací práci ( min )
   * @param delta Množství konvenčního CO
   * @param s Světlý průřez raženého větraného díla ( m )
   * @param qh Měrná hmotnost horniny ( kg m-3 )
   * @param lv Délka zabírky
   * @returns {number}
   */
  qv3Saci (a, t, delta, s, qh, lv) {
    a = Number(a)
    t = Number(t)
    delta = Number(delta)
    s = Number(s)
    qh = Number(qh)
    lv = Number(lv)
    let i = Math.sqrt(s)
    let ii = (delta * i) / (qh * lv)
    let iii = Math.sqrt(ii)

    let part1 = (9.417 * a) / t

    let result = part1 * iii
    if (isNaN(result)) {
      return 0.0
    }
    return result
  }

  /**
   * Qv5 - Ředění zplodin vznětových motorů
   *
   * @param n Instalovaný výkon motoru
   * @param f Faktor průměrného vytížení ( 0,3 - převážná rovina, 0,45 - úklon v převážné části dráhy, 0,6 - silný úklon dráhy)
   * @param q Specifický objemový průtok větrů k ředění
   * @returns {number}
   */
  qv5 (n, f, q) {
    n = Number(n)
    f = Number(f)
    q = Number(q)
    let result = n * f * q
    if (isNaN(result)) {
      return 0.0
    }
    return result
  }
}
