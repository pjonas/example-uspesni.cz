/**
 * defiinice Odporem
 * @type {{getValue: (function(): *), setActive: byResists.setActive, setValue: byResists.setValue, getActive: (function(): *)}}
 */
let byResists = {
  setValue: function (value) {
    this.value = value
  },
  getValue: function () {
    return this.value
  },
  setActive: function (value) {
    this.active = value
  },
  getActive: function () {
    return this.active
  }
}
/**
 * definice Lambdou
 *
 * @type {{getDiameter: (function(): *), getLambda: (function(): *), setDiameter: byLambda.setDiameter, setActive: byLambda.setActive, setRho: byLambda.setRho, setLambda: byLambda.setLambda, getActive: (function(): *), getRho: (function(): *)}}
 */
let byLambda = {
  setDiameter: function (value) {
    this.diameter = value
  },
  getDiameter: function () {
    return this.diameter
  },
  setLambda: function (value) {
    this.lambda = value
  },
  getLambda: function () {
    return this.lambda
  },
  setRho: function (value) {
    this.rho = value
  },
  getRho: function () {
    return this.rho
  },
  setActive: function (value) {
    this.active = value
  },
  getActive: function () {
    return this.active
  }
}
/**
 * definice vyberem z databaze
 *
 * @type {{setName: bySelect.setName, getResists: (function(*): []|*[]), getName: (function(): *), setActive: bySelect.setActive, setResists: bySelect.setResists, getVariant: (function(): *), setDimension: bySelect.setDimension, getDimension: (function(): *), setVariant: bySelect.setVariant, getActive: (function(): *)}}
 */
let bySelect = {
  setName: function (name) {
    this.name = name
  },
  getName: function () {
    return this.name
  },
  setDimension: function (dimension) {
    this.dimension = dimension
  },
  getDimension: function () {
    return this.dimension
  },
  setVariant: function (variant) {
    this.variant = variant
  },
  getVariant: function () {
    return this.variant
  },
  setActive: function (value) {
    this.active = value
  },
  getActive: function () {
    return this.active
  },
  setResists: function (resists) {
    this.resists = resists
  },
  getResists: function (resists) {
    if (typeof this.resists === 'undefined') {
      this.resists = []
    }
    return this.resists
  }
}
/**
 * Jednotliva Lutna a jeji definice
 *
 * @type {{setLength: Lutna.setLength, setByLambda: Lutna.setByLambda, toJSON(): {byLambda: *, byResist: *, computedResist: *, length: *, bySelect: *}, getComputedResist: (function(): number), setComputedResist: Lutna.setComputedResist, getByResist: (function(): any | {getValue: (function(): *), setActive: byResists.setActive, setValue: byResists.setValue, getActive: (function(): *)}), getLength: (function(): number), setByResist: Lutna.setByResist, getBySelect: (function(): any | {setName: bySelect.setName, getResists: ((function(*): *[])|*[]), getName: (function(): *), setActive: bySelect.setActive, setResists: bySelect.setResists, getVariant: (function(): *), setDimension: bySelect.setDimension, getDimension: (function(): *), setVariant: bySelect.setVariant, getActive: (function(): *)}), getByLambda: (function(): any | {getDiameter: (function(): *), getLambda: (function(): *), setDiameter: byLambda.setDiameter, setActive: byLambda.setActive, setRho: byLambda.setRho, setLambda: byLambda.setLambda, getActive: (function(): *), getRho: (function(): *)}), setBySelect: Lutna.setBySelect}}
 */
let Lutna = {
  setLength: function (value) {
    this.length = parseInt(value)
  },
  getLength: function () {
    if (typeof this.length === 'undefined') {
      this.length = 0
    }
    return this.length
  },
  setComputedResist: function (value) {
    this.computedResist = parseFloat(value)
  },
  getComputedResist: function () {
    if (typeof this.computedResist === 'undefined') {
      this.computedResist = 0
    }
    return this.computedResist
  },
  setByResist: function (value) {
    this.byResist = Object.create(byResists)
    this.byResist.setValue(parseFloat(value))
    this.byResist.setActive(true)
  },
  getByResist: function () {
    if (typeof this.byResist === 'undefined') {
      this.byResist = Object.create(byResists)
      this.byResist.setValue(0)
      this.byResist.setActive(false)
    }
    return this.byResist
  },
  setByLambda: function (rho, lambda, diameter) {
    this.byLambda = Object.create(byLambda)
    this.byLambda.setRho(rho)
    this.byLambda.setLambda(lambda)
    this.byLambda.setDiameter(diameter)
    this.byLambda.setActive(true)
  },
  getByLambda: function () {
    if (typeof this.byLambda === 'undefined') {
      this.byLambda = Object.create(byLambda)
      this.byLambda.setRho(0)
      this.byLambda.setLambda(0)
      this.byLambda.setDiameter(0)
      this.byLambda.setActive(false)
    }
    return this.byLambda
  },
  setBySelect: function (name, variant, dimension) {
    this.bySelect = Object.create(bySelect)
    this.bySelect.setVariant(variant)
    this.bySelect.setName(name)
    this.bySelect.setDimension(dimension)
    this.bySelect.setActive(true)
  },
  getBySelect: function () {
    if (typeof this.bySelect === 'undefined') {
      this.bySelect = Object.create(bySelect)
      this.bySelect.setActive(false)
    }
    return this.bySelect
  },
  toJSON () {
    return {
      byResist: this.getByResist(),
      byLambda: this.getByLambda(),
      bySelect: this.getBySelect(),
      length: this.getLength(),
      computedResist: this.getComputedResist()
    }
  }
}

export default Lutna
